import { render, screen } from "@testing-library/react-native";
import { fireEvent } from "@testing-library/react-native/build";
import App from "./App";

describe("The application", () => {
  it("should display the title of the web page", () => {
    render(<App />);

    const webview = screen.getByTestId("webview");

    fireEvent(webview, "message", {
      nativeEvent: { data: "Tyler Williams · GitLab" },
    });

    const title = screen.getByText(
      "The title tag of this website is: Tyler Williams · GitLab"
    );

    expect(title).toBeTruthy();
  });
});
