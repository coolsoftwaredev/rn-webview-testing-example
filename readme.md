# React Native WebView Testing Example

An example repo for a blog post about testing React Native WebView with React Native Testing Library.

Blog post: [https://coolsoftware.dev/testing-react-native-webview-with-react-native-testing-library/](https://coolsoftware.dev/testing-react-native-webview-with-react-native-testing-library/)

## Setup

1. Clone the repo
1. `yarn install` to install dependencies

## Manual QA

1. `yarn expo start` to start the simulator
1. Choose an Android or iOS simulator, whichever you prefer
1. Play around with the code or the app and see if it works as expected.

## Testing with React Native Testing Library

To run the RNTL tests, run `yarn test`

Everything should be passing! Play around with the code in `App.js` to see if you can get it to break and understand why. Write some more tests in `App.spec.js` to see if you can build out some additional WebView functionality with tests.
