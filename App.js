import { useState } from "react";
import { SafeAreaView, StyleSheet, Text, View } from "react-native";
import { WebView } from "react-native-webview";

const injectedJavaScript = `(function() {
  const title = document.title
  window.ReactNativeWebView.postMessage(title);
})()`;

export default function App() {
  const [webPagetitle, setWebPageTitle] = useState("");

  const handleMessage = (event) => {
    setWebPageTitle(event.nativeEvent.data);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.webviewContainer}>
        <WebView
          source={{ uri: "https://gitlab.com/coolsoftwaretyler" }}
          injectedJavaScript={injectedJavaScript}
          onMessage={handleMessage}
          testID="webview"
        />
      </View>
      <View style={styles.infoPane}>
        <Text>The title tag of this website is: {webPagetitle}</Text>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  webviewContainer: {
    flex: 1,
    width: "100%",
  },
  infoPane: {
    flex: 1,
    justifyContent: "center",
  },
});
